$(function () {

    var filterList = {

        init: function () {

            // MixItUp plugin
            // http://mixitup.io
            $('#portfoliolist').mixitup({
                targetSelector: '.portfolio',
                filterSelector: '.filter',
                effects: ['fade'],
                easing: 'snap',
                // call the hover effect
                onMixEnd: filterList.hoverEffect()
            });

        },

        hoverEffect: function () {

            // Simple parallax effect
            $('#portfoliolist .portfolio').hover(
                function () {
                    $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
                    $(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');
                },
                function () {
                    $(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
                    $(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');
                }
            );

        }

    };

    // Run the show!
    filterList.init();

});

$(document).ready(function () {

	Order.init();
	jQuery.ias({
		container: ".main-list",
		item: ".list-item",
		pagination: ".main-paging",
		next: ".main-paging a.loadmore",
		loader: "<img src='/skin/frontend/thefrenchlifestyle/default/images/ajax-loader.gif' style='margin-left:48%'/>",
		triggerPageThreshold: 0
	});

});