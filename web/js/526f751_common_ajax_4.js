var CommonAjax = {
    postRequest: function (requestUrl, postData, callback) {
        $.ajax({
            method: 'POST',
            url: requestUrl,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(postData),
            beforeSend: function () {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        'z-index': 2000
                    }
                });

                jQuery('.blockOverlay').css('z-index', 2000);

                jQuery('.blockPage h1').replaceWith(function () {
                    return "<h3>" + $(this).html() + "</h3>";
                });
            }
        })
            .done(function (data) {
                callback(data);
            })
            .fail(function () {
                console.log('System error');
            })
            .complete(function () {
                setTimeout($.unblockUI, 100);
            });
    },

    postFormRequest: function (requestUrl, postData, callback) {
        $.ajax({
            method: 'POST',
            url: requestUrl,
            data: postData,
            beforeSend: function () {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        'z-index': 2000
                    }
                });

                jQuery('.blockOverlay').css('z-index', 2000);

                jQuery('.blockPage h1').replaceWith(function () {
                    return "<h3>" + $(this).html() + "</h3>";
                });
            }
        })
            .done(function (data) {
                callback(data);
            })
            .fail(function () {
                console.log('System error');
            })
            .complete(function () {
                setTimeout($.unblockUI, 100);
            });
    },

    postFileRequest: function (requestUrl, postData, callback){
        $.ajax({
            method: 'POST',
            url: requestUrl,
            data: postData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        'z-index': 2000
                    }
                });

                jQuery('.blockOverlay').css('z-index', 2000);

                jQuery('.blockPage h1').replaceWith(function () {
                    return "<h3>" + $(this).html() + "</h3>";
                });
            }
        })
            .done(function (data) {
                callback(data);
            })
            .fail(function () {
                console.log('System error');
            })
            .complete(function () {
                setTimeout($.unblockUI, 100);
            });
    },

    getRequest: function (requestUrl, callback, async, blockUI) {
        if(typeof async == 'undefined'){
            async = true;
        }

        if(typeof blockUI == 'undefined'){
            blockUI = false;
        }

        $.ajax({
            method: 'GET',
            url: requestUrl,
            dataType: 'json',
            async : async,
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                if(blockUI){
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff',
                            'z-index': 2000
                        }
                    });

                    jQuery('.blockOverlay').css('z-index', 2000);

                    jQuery('.blockPage h1').replaceWith(function () {
                        return "<h3>" + $(this).html() + "</h3>";
                    });
                }
            }
        })
            .done(function (data) {
                callback(data);
            })
            .fail(function () {
                console.log('System error');
            })
            .complete(function () {
                if(blockUI){
                    setTimeout($.unblockUI, 100);
                }
            });

    }
};
