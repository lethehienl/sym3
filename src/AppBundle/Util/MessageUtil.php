<?php

namespace AppBundle\Util;

class MessageUtil
{
  const SUCCESS = 200;
  const FAIL = 400;
  const VALIDATE_FAIL = 1040;

  public static function formatMessage($code, $message, $data = null)
  {
    $output = array('code' => $code, 'message' => $message);

    if (isset($data)) {
      $output['data'] = $data;
    }

    return $output;
  }
}