<?php

namespace AppBundle\Util;

class StatusUtil
{
  const STATUS_ACTIVE = 1;
  const STATUS_INACTIVE = 0;

  const STATUS_ACTIVE_TITLE = 'Active';
  const STATUS_INACTIVE_TITLE = 'Inactive';

  const BASIC_STATUS = array(
    self::STATUS_ACTIVE_TITLE => self::STATUS_ACTIVE,
    self::STATUS_INACTIVE_TITLE => self::STATUS_INACTIVE,
  );

  const BASIC_STATUS_FLIP = array(
    self::STATUS_ACTIVE => self::STATUS_ACTIVE_TITLE,
    self::STATUS_INACTIVE => self::STATUS_INACTIVE_TITLE,
  );

  public static function getStatus($statusId)
  {
    if($statusId == self::STATUS_ACTIVE) {
      return self::STATUS_ACTIVE_TITLE;
    }

    return self::STATUS_INACTIVE_TITLE;
  }
}