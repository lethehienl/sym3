<?php

namespace AppBundle\Util;

class PagingUtil
{
  const PAGE_PARAMETER = 'page';
  const DEFAULT_PAGE = 1;
  const DEFAULT_ITEMS_PERPAGE = 12;
  const MAX_ITEMS_PERPAGE = 100;
}