<?php

namespace AppBundle\Services;

use AppBundle\Util\PagingUtil;
use AppBundle\Util\ServiceUtil;
use AppBundle\Util\SessionUtil;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AppService
{
	private $loginUser;
	private $request;
	private $entityManager;
	private $container;
	private $paginator;
	private $translator;

	public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, TokenStorageInterface $tokenStorage, ContainerInterface $container)
	{
		$this->container = $container;
		$token = $tokenStorage->getToken();
		$this->entityManager = $entityManager;
		$this->request = $requestStack->getCurrentRequest();

		if (!empty($token)) {
			$this->loginUser = $token->getUser();
		}

		$this->translator = $this->container->get(ServiceUtil::TRANSLATOR_SERVICE);

	}

	public function getLoginUser()
	{
		return $this->loginUser;
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function getEntityManager()
	{
		return $this->entityManager;
	}

	public function getContainer()
	{
		return $this->container;
	}

	public function getTranslator()
	{
		return $this->translator;
	}

	public function getPaginator()
	{
		return $this->paginator;
	}

	public function getCurrentEventPlace()
	{
		return $this->request->getSession()->get(SessionUtil::EVENT_PLACE_SESSION_NAME);
	}

	public function getFileSystem()
	{
		return $this->getContainer()->getParameter('filesystem');
	}

	public function getFileService()
	{
		$fileSystem = $this->getFileSystem();
		return $this->container->get($fileSystem);
	}

	/**
	 *
	 * @param ex: $entity = OrderBundle:Order
	 * @param  ex: $raw= array('key'=>'value')
	 * @return array
	 */
	/*public function getItems($entity, $raw= array(), $sort=array())
	{
		$page = $this->getRequest()->get(PagingUtil::PAGE_PARAMETER);
		$page = empty($page) ? PagingUtil::DEFAULT_PAGE : $page;
		$limit = PagingUtil::DEFAULT_ITEMS_PERPAGE;

		$repo = $this->getEntityManager()->getRepository($entity);

		$result = $repo->findBy($raw,$sort,$limit, $limit*($page -1));
		return $result;
	}*/
}