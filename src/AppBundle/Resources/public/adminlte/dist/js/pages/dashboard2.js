function getValueAt(column, dataTable, row) {
  var data = dataTable.getFormattedValue(row, column);
  var predictData = dataTable.getFormattedValue(row, column + 1);

  var percent = parseInt(data) / parseInt(predictData) * 100;
  return percent.toFixed(0) + '%';
}

$(function () {

  'use strict';

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawVisualization);

  function drawVisualization() {

    var rawData = [
      ['Month', 'Applications', 'Target'],
      ['Jan',     356789,           363979],
      ['Feb',     545900,           484000],
      ['Mar',     427896,           400000],
      ['Apr',     376789,           360000],
      ['May',     320345,           380000],
      ['Jun',     0,                389989],
      ['Jul',     0,                389989],
      ['Aug',     0,                389989],
      ['Sep',     0,                389989],
      ['Oct',     0,                389989],
      ['Nov',     0,                389989],
      ['Dec',     0,                389989],
    ];

    var data = google.visualization.arrayToDataTable(rawData);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
      { calc: getValueAt.bind(undefined, 1),
        sourceColumn: 1,
        type: "string",
        role: "annotation" },
      2]);

    var options = {
      title : 'Do it badly, do it slowly, do it fearfully, do it any way you have to, but do it!!!',
      vAxis: {title: 'Total'},
      hAxis: {title: 'Month'},
      seriesType: 'bars',
      series: {1: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(view, options);

    $('.monthly-data .info-box').hide();

    var demoData = rawData[1][1];
    var targetData = rawData[1][2];

    var ratio = demoData / targetData * 100;
    ratio = ratio.toFixed(2);

    if(ratio >= 110){
      $('.monthly-data .target-1 .info-box-number').text(demoData);
      $('.monthly-data .target-1 .info-percent').text(ratio+'%');
      $('.monthly-data .target-1').show();
    }
    if(ratio >= 100 && ratio < 110){
      $('.monthly-data .target-2 .info-box-number').text(demoData);
      $('.monthly-data .target-2 .info-percent').text(ratio+'%');
      $('.monthly-data .target-2').show();
    }
    if(ratio >= 85 && ratio < 100){
      $('.monthly-data .target-3 .info-box-number').text(demoData);
      $('.monthly-data .target-3 .info-percent').text(ratio+'%');
      $('.monthly-data .target-3').show();
    }

    if(ratio < 85){
      $('.monthly-data .target-4 .info-box-number').text(demoData);
      $('.monthly-data .target-4 .info-percent').text(ratio+'%');
      $('.monthly-data .target-4').show();
    }
  }

});
