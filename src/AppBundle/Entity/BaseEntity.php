<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\MappedSuperclass()
*/
class BaseEntity
{
  public function __construct()
  {
    $currentDate = new \DateTime();
    $this->created = $currentDate;
    $this->changed = $currentDate;
  }

  /**
   * @ORM\Column(name="created_by", type="integer", nullable=true)
   */
  private $createdBy;

  /**
   * @ORM\Column(name="updated_by", type="integer", nullable=true)
   */
  private $updatedBy;

  /**
   * @ORM\Column(name="created", type="datetime", nullable=true)
   */
  private $created;

  /**
   * @ORM\Column(name="changed", type="datetime", columnDefinition="DATETIME on update CURRENT_TIMESTAMP")
   */
  private $changed;

  public function getCreatedBy()
  {
    return $this->createdBy;
  }

  public function setCreatedBy($createdBy)
  {
    $this->createdBy = $createdBy;
    return $this;
  }

  public function getUpdatedBy()
  {
    return $this->updatedBy;
  }

  public function setUpdatedBy($updatedBy)
  {
    $this->updatedBy = $updatedBy;
    return $this;
  }

  public function getCreated()
  {
    return $this->created;
  }

  public function setCreated($created)
  {
    $this->created = $created;
    return $this;
  }

  public function getChanged()
  {
    return $this->changed;
  }

  public function setChanged($changed)
  {
    $this->changed = $changed;
    return $this;
  }
}

