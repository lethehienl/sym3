<?php

namespace AppBundle\Controller;

use AppBundle\Util\ServiceUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class AdminController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeAction()
    {
        return $this->render('@App/back_office/admin.html.twig');
    }

  /**
   * @Route("/admin", name="adminpage")
   */
  public function indexAction()
  {
    return $this->render('@App/back_office/admin.html.twig');
  }

    /**
     * @Route("/admin/account", name="admin_account")
     */
    public function accountManagementAction(Request $request)
    {
        return $this->render('@App/back_office/admin_account.html.twig', array());
    }

    /**
     * @Route("/admin/payment", name="admin_payment")
     */
    public function paymentInfoAction(Request $request)
    {
        return $this->render('@App/back_office/admin_payment.html.twig', array());
    }

    /**
     * @Route("/admin-demo", name="admin_demo")
     */
    public function adminDemoAction(Request $request)
    {
        return $this->render('@App/back_office/admin_demo.html.twig', array());
    }

}
