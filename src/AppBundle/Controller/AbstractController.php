<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Util\SessionUtil;
use AppBundle\Util\ServiceUtil;


class AbstractController extends Controller
{
  public function getUser()
  {
    return $this->get('security.token_storage')->getToken()->getUser();
  }

  public function getAuthenticatedUser()
  {
    if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
      return $this->getUser();
    }
    return null;
  }

  public function getActiveEventPlace(){
    return $this->get(ServiceUtil::SESSION_SERVICE)->get(SessionUtil::EVENT_PLACE_SESSION_NAME);
  }
}
