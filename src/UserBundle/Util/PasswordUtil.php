<?php

namespace UserBundle\Util;


use UserBundle\Entity\User;

class PasswordUtil
{
  const MINIMUM_PASSWORD_LENGTH = 6;
  const MAX_PASSWORD_LENGTH = 10;

  public function validatePassword(User $user)
  {
    if (empty($user->getPassword())) {
      throw new \Exception('user.password_empty');
    }

    if (strlen($user->getPassword()) < self::MINIMUM_PASSWORD_LENGTH) {
      throw new \Exception('user.password_too_short');
    }
  }

  public static function generateRandomPassword()
  {
    return md5(uniqid(rand(PasswordUtil::MINIMUM_PASSWORD_LENGTH, PasswordUtil::MAX_PASSWORD_LENGTH), true));
  }
}