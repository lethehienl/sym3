<?php

namespace UserBundle\Util;


use AppBundle\Util\MessageUtil;

class UserUtil
{
  const ACTIVE = 1;
  const INACTIVE = 0;

  const FACEBOOK = 'facebook';
  const GOOGLE = 'google';
  const LINKEDIN = 'linkedin';

  const STAR = [0,1,2,3,4,5];

  public static function getSocialEmail($data, $type)
  {
    switch ($type) {
      case self::FACEBOOK:
      case self::GOOGLE:
        $email = $data['email'];
        break;
      case self::LINKEDIN:
        $email = $data['emailAddress'];
        break;
      default:
        $email = $data['email'];
        break;
    }

    if(empty($email)) {
      throw new \Exception(MessageUtil::SOCIAL_NO_EMAIL_MESSAGE, MessageUtil::SOCIAL_NO_EMAIL_CODE);
    }

    return $email;
  }

  public static function getFirstNameAndLastName($fullname)
  {
    $result = [
      'first_name' => '',
      'last_name' => ''
    ];

    $nameArray = explode(' ', $fullname);
    $result['last_name'] = $nameArray[0];
    unset($nameArray[0]);

    $result['first_name'] = implode(' ', $nameArray);
    $result['first_name'] = (empty($result['first_name'])) ? 'Hiro' : $result['first_name'];

    return $result;
  }
}