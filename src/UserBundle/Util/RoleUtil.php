<?php
namespace UserBundle\Util;

class RoleUtil
{
  const ROLE_SUPERADMIN = 'ROLE_SUPERADMIN';
  const ROLE_HIRO = 'ROLE_EMPLOYEE';
  const ROLE_ANONYMOUSLY = 'IS_AUTHENTICATED_ANONYMOUSLY';

  const ROLE_SUPERADMIN_TITLE = 'Admin';
  const ROLE_HIRO_TITLE = 'Employee';

  const ROLE_SUPERADMIN_CODE = 1;
  const ROLE_HIRO_CODE = 2;

  const ROLE_TITLES = array(
    self::ROLE_SUPERADMIN_CODE => self::ROLE_SUPERADMIN_TITLE,
    self::ROLE_HIRO_CODE => self::ROLE_HIRO_TITLE,
  );

  const ROLES = array(
    self::ROLE_SUPERADMIN_CODE => self::ROLE_SUPERADMIN,
    self::ROLE_HIRO_CODE => self::ROLE_HIRO,
  );

  const ADMIN_ROLES = array(
    self::ROLE_HIRO_TITLE => self::ROLE_HIRO_CODE,
    self::ROLE_SUPERADMIN_TITLE => self::ROLE_SUPERADMIN_CODE
  );
}