<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use AppBundle\Util\StatusUtil;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
  public function getUsers($text, $roleCode = [])
  {
    $queryBuilder = $this->createQueryBuilder('user');
    $queryBuilder
      ->select('user')
      ->orderBy('user.created', 'DESC');



    if(!empty($text)) {
      $queryBuilder->where('user.userName like :username')
        ->setParameter('username', '%' . $text . '%');
      $queryBuilder->orWhere('user.email like :email')
        ->setParameter('email', '%' . $text . '%');
    }

    if ($roleCode) {
      $queryBuilder
        ->andWhere('user.roleMask in (:roleCode)')
        ->setParameter('roleCode', $roleCode);
    }

    return $queryBuilder;
  }

  public function findOneByUsernameOrEmailExcept($username, $email, $exceptUser)
  {
    $queryBuilder = $this->createQueryBuilder('user');
    $queryBuilder
      ->select('user')
      ->where('user.userName = :username')
      ->orWhere('user.email = :email')
      ->setParameter('username', $username)
      ->setParameter('email', $email);

    $exceptId = $exceptUser->getId();
    if ($exceptId) {
      $queryBuilder->andWhere('user.id != :exceptId')->setParameter(':exceptId', $exceptId);
    }

    return $queryBuilder->getQuery()->getResult();
  }

  public function searchByNameOrEmail($text, $roleCode = [])
  {
    $queryBuilder = $this->createQueryBuilder('user');
    $queryBuilder
      ->select('user');

    if(!empty($text)) {
      $queryBuilder->where('user.userName like %:username%')
        ->setParameter('username', $text);

      $queryBuilder->orWhere('user.email like %:email%')
        ->setParameter('email', $text);
    }
    $queryBuilder->setParameter('roleCode', $roleCode);

    return $queryBuilder;
  }

  public function loadUserByUsername($username)
  {
    return $this->findOneBy(array('userName' => $username, 'status' => StatusUtil::STATUS_ACTIVE));
  }

  public function getTotalUsers() {
    return $this->createQueryBuilder('user')
      ->select('count(user.id)')
      ->getQuery()
      ->getSingleScalarResult();
  }
}
