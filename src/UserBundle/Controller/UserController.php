<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
//    /**
//     * @Route("/register")
//     */
//    public function registerAction(Request $request)
//    {
//        return $this->render('UserBundle:anon:register.html.twig');
//    }
//
//    /**
//     * @Route("/login")
//     */
//    public function loginAction(Request $request)
//    {
//        return $this->render('UserBundle:anon:login.html.twig');
//    }
//
//    /**
//     * @Route("/forgot")
//     */
//    public function forgotPasswordAction(Request $request)
//    {
//        return $this->render('UserBundle:anon:forgot_password.html.twig');
//    }
//
//    /**
//     * @Route("/change_password")
//     */
//    public function changePasswordAction(Request $request)
//    {
//        return $this->render('UserBundle:anon:change_password.html.twig');
//    }

    /**
     * @Route("/admin/user", name="admin_user_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $roles = [RoleUtil::ROLE_SUPERADMIN_CODE, RoleUtil::ROLE_EMPLOYER_CODE];
        $data = $this->get(ServiceUtil::USER_SERVICE)->getUsers($roles);
        $filterRoles = $request->query->get('filter_roles');

        $requestData = array(
            'filter_roles' => $filterRoles
        );

        $form = $this->createForm(new BasicSearchType($requestData));
        $form->handleRequest($request);

        $data = array_merge($data, array('form' => $form->createView()));
        return $this->render('UserBundle::admin/index.html.twig', $data);
    }

    /**
     * @Route("/admin/user/create", name="admin_user_new")
     * @Method({"GET", "POST"})
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserType(true), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = $request->getSession();

            try {
                $this->get(ServiceUtil::USER_SERVICE)->saveUser($user);
                $session->getFlashBag()->add(MessageUtil::SUCCESS_KEY, $this->get(ServiceUtil::TRANSLATOR_SERVICE)->trans('user.success'));

                return $this->redirect($this->generateUrl('admin_user_index'));
            } catch (\Exception $ex) {
                $session->getFlashBag()->add(MessageUtil::ERROR_KEY, $this->get(ServiceUtil::TRANSLATOR_SERVICE)->trans($ex->getMessage()));
            }
        }

        $data = array('form' => $form->createView());
        return $this->render('UserBundle::admin/form.html.twig', $data);
    }

    /**
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $user = $this->get(ServiceUtil::USER_SERVICE)->getUser($id);
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = $request->getSession();

            try {
                $this->get(ServiceUtil::USER_SERVICE)->saveUser($user);
                $session->getFlashBag()->add(MessageUtil::SUCCESS_KEY, $this->get(ServiceUtil::TRANSLATOR_SERVICE)->trans('user.success'));

                return $this->redirect($this->generateUrl('admin_user_index'));
            } catch (\Exception $ex) {
                $session->getFlashBag()->add(MessageUtil::ERROR_KEY, $this->get(ServiceUtil::TRANSLATOR_SERVICE)->trans($ex->getMessage()));
            }
        }

        $data = array('form' => $form->createView());
        return $this->render('UserBundle::admin/form.html.twig', $data);
    }

    /**
     * FRONT OFFICE
     */

    /**
     * @Route("/change-password", name="user_change_password")
     * @Method({"GET","POST"})
     */
    public function changePasswordAction()
    {
        return $this->render('UserBundle::anon/change_password.html.twig');
    }

    /**
     * @Route("/register", name="user_register")
     * @Method({"GET","POST"})
     */
    public function registerAction()
    {
        return $this->render('UserBundle::anon/register.html.twig');
    }

    /**
     * @Route("/forgot-password", name="user_forgot_password")
     * @Method({"GET","POST"})
     */
    public function forgotAction()
    {
        return $this->render('UserBundle::anon/forgot_password.html.twig');
    }

    /**
     * @Route("/user-profile", name="user_profile")
     * @Method({"GET"})
     */
    public function myProfileAction()
    {
        return $this->render('UserBundle::user/profile.html.twig');
    }

    /**
     * END FRONT OFFICE
     */
}
