<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use UserBundle\Util\RoleUtil;

class BasicSearchType extends AbstractType
{
  protected $requestData;

  public function __construct($requestData)
  {
    $this->requestData = $requestData;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('search_text', TextType::class, array("attr" =>
        array('placeholder' => 'Search ...', "class" => "mb1", 'place'),
        'required' => false,
      ))
      ->add('filter_roles', ChoiceType::class, array(
        'choices' => RoleUtil::ADMIN_ROLES,
        'choices_as_values' => true,
        'required' => false,
        "attr" => array("class" => "form-control col-md-4 col-xs-12"),
        "data" => @$this->requestData['filter_roles'],
        "empty_value" => '--All roles--'
    ))
      ->add('search', SubmitType::class);
  }

  public function getName() {
    return '';
  }
}
