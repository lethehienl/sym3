<?php

namespace UserBundle\Form;

use AppBundle\Util\StatusUtil;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Util\RoleUtil;

class UserType extends AbstractType
{
  private $create;

  public function __construct($create = false)
  {
    $this->create = $create;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('firstName', TextType::class)
      ->add('lastName', TextType::class)
      ->add('email', TextType::class, array("attr" => array(
        "pattern" => ".+@.+\..+",
        'autocomplete' => 'off')))
      ->add('userName', TextType::class)
      ->add('roleMask', ChoiceType::class, array(
        'choices' => RoleUtil::ADMIN_ROLES,
        'choices_as_values' => true
      ))
      ->add('status', ChoiceType::class, array(
        'choices' => StatusUtil::BASIC_STATUS,
        'choices_as_values' => true
      ))
      ->add('image', FileType::class, array(
        'label' => 'File',
        'data_class' => null,
        'required' => false
      ));
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'UserBundle\Entity\User'
    ));
  }
}