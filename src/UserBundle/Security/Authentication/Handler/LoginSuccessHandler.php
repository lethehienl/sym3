<?php
namespace UserBundle\Security\Authentication\Handler;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
  protected $router;

  public function __construct(Router $router)
  {
    $this->router = $router;
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token)
  {
    $response = new RedirectResponse($this->router->generate('/'));
    return $response;
  }
}