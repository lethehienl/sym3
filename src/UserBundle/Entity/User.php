<?php

namespace UserBundle\Entity;

use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use UserBundle\Util\RoleUtil;
use XSecurityBundle\Entity\Authorization;
use XSecurityBundle\Entity\Client;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User extends BaseEntity implements AdvancedUserInterface, \Serializable
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="username", type="string", length=255, unique=true)
   */
  private $userName;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=255, unique=true)
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="password", type="string", length=255, nullable=true)
   */
  private $password;

  private $repeatPassword;

  /**
   * @var string
   *
   * @ORM\Column(name="fullName", type="string", length=255)
   */
  private $fullName;

  /**
   * @var string
   *
   * @ORM\Column(name="phoneNo", type="string", length=100)
   */
  private $phoneNumber;

  /**
   * @var int
   *
   * @ORM\Column(name="status", type="integer")
   */
  private $status;

  /**
   * @var int
   *
   * @ORM\Column(name="rolemask", type="integer")
   */
  private $roleMask;

  /**
   * @var int
   *
   * @ORM\Column(name="point", type="integer")
   */
  private $point;

  /**
   * @var int
   *
   * @ORM\Column(name="password_token", type="string", length=512, nullable=true)
   */
  private $passwordToken;





  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getUserName()
  {
    return $this->userName;
  }

  /**
   * @param string $userName
   */
  public function setUserName($userName)
  {
    $this->userName = $userName;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password = $password;
  }

  /**
   * @return mixed
   */
  public function getRepeatPassword()
  {
    return $this->repeatPassword;
  }

  /**
   * @param mixed $repeatPassword
   */
  public function setRepeatPassword($repeatPassword)
  {
    $this->repeatPassword = $repeatPassword;
  }

  /**
   * @return int
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param int $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * @return int
   */
  public function getRoleMask()
  {
    return $this->roleMask;
  }

  /**
   * @param int $roleMask
   */
  public function setRoleMask($roleMask)
  {
    $this->roleMask = $roleMask;
  }

  public function getRoleTitles(){
    $roleTitles = RoleUtil::ROLE_TITLES;
    return array($roleTitles[$this->roleMask]);
  }

  public function getRoles()
  {
    $roles = RoleUtil::ROLES;
    return array($roles[$this->roleMask]);
  }

  public function getSalt()
  {
    return null;
  }

  public function eraseCredentials()
  {
    return null;
  }

  public function getRoleName()
  {
    $roleTitles = RoleUtil::ROLES;
    return $roleTitles[$this->roleMask];
  }

  public function serialize()
  {
    return serialize(array(
      $this->id,
      $this->userName,
      $this->password,
      $this->status,
    ));
  }

  public function unserialize($serialized)
  {
    list (
      $this->id,
      $this->userName,
      $this->password,
      $this->status,
      ) = unserialize($serialized);
  }

  /**
   * @return string
   */
  public function getFullName()
  {
    return $this->fullName;
  }

  /**
   * @param string $fullName
   */
  public function setFullName($fullName)
  {
    $this->fullName = $fullName;
  }

  /**
   * @return string
   */
  public function getPhoneNumber()
  {
    return $this->phoneNumber;
  }

  /**
   * @param string $phoneNumber
   */
  public function setPhoneNumber($phoneNumber)
  {
    $this->phoneNumber = $phoneNumber;
  }

  /**
   * @return int
   */
  public function getPoint()
  {
    return $this->point;
  }

  /**
   * @param int $point
   */
  public function setPoint($point)
  {
    $this->point = $point;
  }


  /**
   * @return int
   */
  public function getPasswordToken()
  {
    return $this->passwordToken;
  }

  /**
   * @param int $passwordToken
   */
  public function setPasswordToken($passwordToken)
  {
    $this->passwordToken = $passwordToken;
  }


    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return true;
    }

    public function __toString()
    {
	    return $this->fullName;
    }
}
